#!/usr/bin/env python

import argparse
import collections
import itertools
import logging

# Start of the functions used for testing our formula with brute-force solutions

def check_m(dnaseq, m):
    m = int(m)
    ACGTs = set(['A' * m, 'C' * m, 'G' * m, 'T' * m])
    for substring in ACGTs:
        if substring in dnaseq: 
            return False
    return True
def encircle(dnaseq, m):
    ret = dnaseq
    while len(ret) <= 2*(m + len(dnaseq)):
        ret = ret + dnaseq
    return ret
def brute_force_filt_by_m(m,n):
    dnaseqs = [''.join(x) for x in itertools.product('ACGT', repeat = n)]
    return [x for x in dnaseqs if check_m(x,m)]
def brute_force_filt_by_m_circle(m,n):
    dnaseqs = [''.join(x) for x in itertools.product('ACGT', repeat = n)]
    return [x for x in dnaseqs if check_m(encircle(x, m),m)]
def rc(dnaseq):
    t = {
            'A':'T',
            'T':'A',
            'G':'C',
            'C':'G'
    }
    return ''.join(t[c] for c in dnaseq)[::-1]
def filter_by_rc(dnaseqs):
    return list(set([min(x,rc(x)) for x in dnaseqs]))
def rotate(dnaseq):
    return dnaseq[-1] + dnaseq[:-1]
def filter_by_rotation(dnaseqs):
    visited = set(dnaseqs)
    ret = []
    for dnaseq in dnaseqs:
        y = dnaseq
        ys = [y]
        for i in range(len(dnaseq)):
            y = rotate(y)
            ys.append(y)
        ret.append(min(ys))
    return list(set(ret))
def filter_by_rotation_and_rc(dnaseqs):
    visited = set(dnaseqs)
    ret = []
    for dnaseq in dnaseqs:
        y = dnaseq
        ys = [y,rc(y)]
        for i in range(len(dnaseq)):
            y = rotate(y)
            ys.append(y)
            ys.append(rc(y))
        ret.append(min(ys))
    return list(set(ret))

# End of the functions used for testing ...

DELTA = 1 # conversion of array end indexes from pure math (inclusive) to programming languges (exclusive)

# Helper functions

def intdiv(a,b):
    assert a % b == 0, '{} mod {} === 0 failed'.format(a, b)
    return a // b
    
def GCD(a, b):
    if a < b: return GCD(b, a)
    if b == 0: return a
    return GCD(b, a%b)

def reverseComplement(f,m,n,*args):
    if n % 2 == 1:
        return intdiv(f(m,n,*args), 2) 
    else:
        return intdiv(f(m,n,*args) + f(m,intdiv(n,2),*args), 2)

def FA(n): 
    return 4**n
def FB(m,n,dptable):
    if (m,n) in dptable: return dptable[(m,n)] 
    ret = (4**n) if (n < m) else (3 * sum(FB(m,n-i,dptable) for i in range(1,m-1+DELTA)))
    dptable[(m,n)] = ret
    return ret
def FB1(m,n,dptable):
    if (m,n) in dptable: return dptable[(m,n)]
    if n < m:
        ret = 3 * 4**(n-1)
    elif n == m:
        ret = 3 * 4**(n-1) - 3
    else: 
        ret = (3 * sum(FB1(m,n-i,dptable) for i in range(1,m-1+DELTA)))
    dptable[(m,n)] = ret
    return ret
def FB2L1(m,n,dpL1,dpL3):
    if (m,n) in dpL1: return dpL1[(m,n)]
    if n == 1: 
        ret = 0
    elif n < m:
        ret = 3 * 4**(n-2)
    else: 
        ret = (sum(FB2L3(m,n-i,dpL1,dpL3) for i in range(1,m-1+DELTA)))
    dpL1[(m,n)] = ret
    return ret
def FB2L3(m,n,dpL1,dpL3):
    if (m,n) in dpL3: return dpL3[(m,n)]
    if n == 1:
        ret = 3
    elif n < m:
        ret = 3 * 4**(n-2) * 3
    else: 
        ret = (sum(3*FB2L1(m,n-i,dpL1,dpL3) + 2*FB2L3(m,n-i,dpL1,dpL3) for i in range(1,m-1+DELTA)))
    dpL3[(m,n)] = ret
    return ret
def FB2(m,n,dptable):
    if n <= 0: return 0
    dpL1, dpL3 = dptable
    return FB2L3(m,n,dpL1,dpL3)
def FC(m,n,dptableFB,dptableFB2,dptableFC, allow_cycles = False):
    overcount = (4 if (m > n and not allow_cycles) else 0)
    if (m,n) in dptableFC:
        ret = dptableFC[(m,n)]
        return ret - overcount
    ret = FB(m,n,dptableFB) - 4 *  sum((2*(m-1) - L + 1) * FB2(m,n-L,dptableFB2) for L in range(m, 2*(m-1)+1))
    dptableFC[(m,n)] = ret
    assert ret - overcount >= 0
    return ret - overcount
def FD1(m,n,dptableFB,dptableFB2,dptableFC):
    def rev_complement_adjust(m,n2,dptableFB,dptableFB2,dptableFC):
        if n2 % 2 == 1:
            return intdiv(FC(m,n2,dptableFB,dptableFB2,dptableFC), 2)
        else:
            return intdiv(FC(m,n2,dptableFB,dptableFB2,dptableFC) + FB(m,intdiv(n2,2),dptableFB), 2)
    onlyRC = rev_complement_adjust(m,n,dptableFB,dptableFB2,dptableFC)
    logging.debug(F'  rev_complement_adjust({m},{n}) = {onlyRC}')
    rotations = sum((FC(m,GCD(i,n),dptableFB,dptableFB2,dptableFC) if (GCD(n,i) > 1 or m > n) else 0) for i in range(0,n-1+1))
    if n%2==0: reverse_complementations = intdiv(n*FB(m,intdiv(n,2),dptableFB), 2)
    else: reverse_complementations = 0
    logging.debug('  rotations, reverse_complementations = {}, {}'.format(rotations, reverse_complementations))
    if n % 2 == 0:
        return onlyRC, intdiv(rotations, n), intdiv(rotations + reverse_complementations, 2 * n)
    else:
        return onlyRC, intdiv(rotations, n), intdiv(rotations, 2 * n)
    
def main():
    parser = argparse.ArgumentParser(
        description='This program computes the number of DNA sequences satisfying the following two requirements. \n'
        ' (1) The DNA sequence has exactly n letters. \n'
        ' (2) The DNA sequence does not contain any substring consisting of m consecutive and identical letters. \n'
        'If a DNA sequence A can be obtained by either the rotation or the reverse complementation (RC) of another DNA sequence B, '
        'then A and B are considered to be equivalent to each. ')
    parser.add_argument('-m', type=int, nargs='+', default=[8],
            help='The generated DNA string cannot contain any substring consisting of this many consecutive occurrences of the same letter')
    parser.add_argument('-n', type=int, nargs='+', default=[20],
            help='The number of letters in (or equivalently the length of) the circular DNA string. ')
    parser.add_argument('-T', type=int, default=0,
            help='Test results using brute-force (BF). '
            'Value 0 disables the test, value 1 print BF results only, and value 2 asserts on BF results. '
            '(warning: the test may run very slowly for large value of -n)')
    args = parser.parse_args()
    m, n = args.m, args.n
    
    if args.T > 0:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
        
    mn2result = {}
    for m in args.m:
        dptableFB = {}
        dptableFB1 = {}
        dptableFB2L1 = {}
        dptableFB2L3 = {}
        dptableFB2 = (dptableFB2L1, dptableFB2L3)
        dpTableFC = {}
        mn2result[m] = {}
        for n in args.n:
            if args.T > 0:
                lin_seqs = brute_force_filt_by_m(m,n)
                cir_seqs = brute_force_filt_by_m_circle(m,n)
                lin_seqs_rc = filter_by_rc(lin_seqs)
                cir_seqs_rc = filter_by_rc(cir_seqs)
                cir_seqs_rot = filter_by_rotation(cir_seqs)
                cir_seqs_rot_and_rc = filter_by_rotation_and_rc(cir_seqs)
                logging.debug('BruteForce(m={},n={})=(linear:none={},rc={}  circle:none={},rc={},rot={},rot_and_rc={})'
                    .format(m,n,len(lin_seqs),len(lin_seqs_rc),
                    len(cir_seqs),len(cir_seqs_rc),len(cir_seqs_rot),len(cir_seqs_rot_and_rc)))
            vFB = FB(m,n,dptableFB)
            vFB1 = FB1(m,n,dptableFB1)
            vFB2L1 = FB2L1(m,n,dptableFB2L1,dptableFB2L3)
            vFB2L3 = FB2L3(m,n,dptableFB2L1,dptableFB2L3)
            vFB2 = FB2(m,n,dptableFB2)
            vFC = FC(m,n,dptableFB,dptableFB2,dpTableFC)
            eRC, eRot, eRotRC = FD1(m,n,dptableFB,dptableFB2,dpTableFC)
            
            logging.debug('FB({}, {}) = {}'.format(m, n, vFB))
            logging.debug('FB1({}, {}) = {}'.format(m, n, vFB1))
            logging.debug('FB2L1({}, {}) = {}'.format(m, n, vFB2L1))
            logging.debug('FB2L3({}, {}) = {}'.format(m, n, vFB2L3))
            logging.debug('FB2({}, {}) = {}'.format(m, n, vFB2))
            logging.debug('FC({}, {}) = {}'.format(m, n, vFC))
            logging.info('Finally, FD(m={}, n={}) = {},{},{} (number of reverse-complementation(RC), rotation, and RC-and-rotation equivalent classes)'.format(m, n, eRC, eRot, eRotRC))
            
            if args.T > 1:
                assert vFB == len(lin_seqs)
                assert vFC == len(cir_seqs)
                assert eRC == len(cir_seqs_rc)
                assert eRot == len(cir_seqs_rot)
                assert eRotRC == len(cir_seqs_rot_and_rc)
            
            mn2result[m][n] = eRotRC
    print('% Table-of-results')
    for m in mn2result:
        print('  {} &  '.format(m) + ' & '.join('{}'.format(int(mn2result[m][n])) for n in mn2result[m]) + ' \\\\')
    
if __name__ == '__main__':
    main()
    
