#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

const char *SEPS = ", \t";

typedef uint64_t myuint;

const myuint INIVAL = -1;
const myuint DELTA = 1; // conversion of array end indexes from pure math (inclusive) to programming languges (exclusive)

myuint intdiv(myuint a, myuint b) {
    assert ((a % b == 0) || !fprintf(stderr, "%lu mod %lu == 0 failed", a, b));
    return a / b;
}
myuint GCD(myuint a, myuint b) {
    if (a < b) { return GCD(b, a); }
    if (b == 0) { return a; }
    return GCD(b, a%b);
}
myuint FA(myuint n) {
	myuint ret = 1;
	for (myuint i = 0; i < n; i++) { ret *= 4; }
    return ret;
}
myuint FB(myuint m, myuint n, myuint **dptable) {
    if (dptable[m][n] != INIVAL) { return dptable[m][n]; }
	myuint ret = INIVAL;
	if (n < m) {
		ret = FA(n);
	} else {
		ret = 0;
		for (myuint i = 1; i < m-1+DELTA; i++) {
			ret += 3 * FB(m, n - i, dptable);
		}
	}
    dptable[m][n] = ret;
    return ret;
}
myuint FB1(myuint m, myuint n, myuint **dptable) {
	if (dptable[m][n] != INIVAL) { return dptable[m][n]; }
    myuint ret = INIVAL;
	if (n < m) {
        ret = 3 * FA(n-1);
    } else if (n == m) {
        ret = 3 * FA(n-1) - 3;
    } else {
		ret = 0;
		for (myuint i = 1; i < m-1+DELTA; i++) {
			ret += 3 * FB1(m, n - i, dptable);
		}
	}
    dptable[m][n] = ret;
    return ret;
}

myuint FB2L1(myuint m, myuint n, myuint **dpL1, myuint **dpL3);
myuint FB2L3(myuint m, myuint n, myuint **dpL1, myuint **dpL3);

myuint FB2L1(myuint m, myuint n, myuint **dpL1, myuint **dpL3) {
    if (dpL1[m][n] != INIVAL) { return dpL1[m][n]; }
    myuint ret = INIVAL;
	if (n == 1) {
        ret = 0;
    } else if (n < m) {
        ret = 3 * FA(n-2);
    } else {
        ret = 0;
		for (myuint i = 1; i < m-1+DELTA; i++) {
			ret += FB2L3(m, n - i, dpL1, dpL3);
		}
	}
    dpL1[m][n] = ret;
	return ret;
}

myuint FB2L3(myuint m, myuint n, myuint **dpL1, myuint **dpL3) {
    if (dpL3[m][n] != INIVAL) { return dpL3[m][n]; }
	myuint ret = INIVAL;
    if (n == 1) {
        ret = 3;
	} else if (n < m) {
        ret = 3 * FA(n-2) * 3;
    } else {
        ret = 0;
		for (myuint i = 1; i < m-1+DELTA; i++) {
			ret += 3 * FB2L1(m, n - i, dpL1, dpL3) + 2 * FB2L3(m,n-i,dpL1,dpL3);
		}
    }
	dpL3[m][n] = ret;
    return ret;
}

myuint FB2(myuint m, myuint n, myuint **dpL1, myuint **dpL3) { // kwargs
	if (n <= 0) { return 0;}
    return FB2L3(m,n,dpL1,dpL3);
}
myuint FC(myuint m, myuint n, myuint **dptableFB, myuint **dpL1, myuint **dpL3, myuint **dptableFC) {
    myuint overcount = ((m > n) ? 4 : 0);
	if (dptableFC[m][n] != INIVAL) {
		return dptableFC[m][n] - overcount;	
	}
	myuint ret = FB(m,n,dptableFB);
	for (myuint L = m; L < 2*(m-1)+DELTA; L++) {
		if (n >= L) {
			ret -= 4 * (2*(m-1) - L + DELTA) * FB2(m, n-L, dpL1, dpL3);
		}
	}
	dptableFC[m][n] = ret;
    assert (ret - overcount >= 0);
    return ret - overcount;
}
myuint rev_complement_adjust(myuint m, myuint n2, myuint **dptableFB, myuint **dpL1, myuint **dpL3, myuint **dptableFC) {
	if (n2 % 2 == 1) {
		return intdiv(FC(m,n2,dptableFB,dpL1,dpL3,dptableFC), 2);
	} else {
		return intdiv(FC(m,n2,dptableFB,dpL1,dpL3,dptableFC) + FB(m,intdiv(n2,2),dptableFB), 2);
	}
}
myuint FD1(myuint m, myuint n, myuint **dptableFB, myuint **dpL1, myuint **dpL3, myuint **dptableFC) {
    // myuint onlyRC = rev_complement_adjust(m,n,dptableFB,dpL1,dpL3,dptableFC);
	myuint rotations = 0;
	for (myuint i = 0; i < n-1+DELTA; i++) {
		if ((GCD(n,i) > 1) || (m > n)) {
			rotations += FC(m,GCD(i,n),dptableFB,dpL1,dpL3,dptableFC);
			// fprintf(stderr, "GCD(%lu, %lu) = %lu, rotation =  %lu\n",  i, n, GCD(i,n), rotations);
		}
	}
	myuint reverse_complementations = ((n%2 == 0) ? intdiv(n*FB(m,intdiv(n,2),dptableFB), 2) : 0);
	// fprintf(stderr, "Num_rotations = %lu\n", rotations);
	// fprintf(stderr, "Num_reverse_complementations = %lu\n", reverse_complementations);
    return intdiv(rotations + reverse_complementations, 2 * n);
#if 0
	if (n % 2 == 0) {
        // return onlyRC, intdiv(rotations, n), intdiv(rotations + reverse_complementations, 2 * n)
    } else {
        // return onlyRC, intdiv(rotations, n), intdiv(rotations, 2 * n)
    }
#endif
}

myuint **malloc2d(myuint m, myuint n) {
	myuint **ret = (myuint **) malloc((sizeof(myuint*) * (m+2)));
	for (myuint i = 0; i <= m + 1; i++) {
		ret[i] = (myuint *) malloc((sizeof(myuint) * (n+2)));
		for (myuint j = 0; j <= n + 1; j++) {
			ret[i][j] = INIVAL;
		}
	}
	return ret;
}

void free2d(myuint **arr, myuint m, myuint n) {
	for (myuint i = 0; i <= m + 1; i++) {
		free(arr[i]);
	}
	free(arr);
}

void print2d(const myuint *const* const arr, const char *name, myuint m, myuint n) {
	fprintf(stderr, "%s\n", name);
	for (myuint i = 0; i <= m + 1; i++) {
		for (myuint j = 0; j <= n + 1; j++) {
			fprintf(stderr, "%lu\t", arr[i][j]);
		}
		fprintf(stderr, "\n");
	}
}

myuint get_nints(char *s) {
	myuint ret = 0;
	char *ptr = s;
	char *token = NULL;
	while ((token = strtok_r(ptr, SEPS, &ptr)) != NULL) {
		ret++;
		// fprintf(stderr,"ptr=%s\n",ptr);
	}
	return ret + 1;
}

myuint get_maxint(char *s) {
	myuint ret = 0;
	char *ptr = s;
	char *token = NULL;
	while ((token = strtok_r(ptr, SEPS, &ptr)) != NULL) {
		myuint probsize = atoi(token);
		if (ret < probsize) { ret = probsize; }
		// fprintf(stderr,"ptr=%s\n",ptr);
	}
	return ret + 1;
}


int main(int argc, char **argv) {
	const char description[] = ("This program computes the number of DNA sequences satisfying the following two requirements. \n"
        " (1) The DNA sequence has exactly n letters. \n"
        " (2) The DNA sequence does not contain any substring consisting of m consecutive and identical letters. \n"
        "If a DNA sequence A can be obtained by either the rotation or the reverse complementation (RC) of another DNA sequence B, "
        "then A and B are considered to be equivalent to each. \n");
	printf(description);
	const char *mstring = "8";
	const char *nstring = "20";
	for (int argidx = 0; argidx < argc; argidx++) {
		if (!strcmp("-m", argv[argidx])) {  mstring = (argv[argidx+1]); }
		if (!strcmp("-n", argv[argidx])) {  nstring = (argv[argidx+1]); }
		if (!strcmp("-h", argv[argidx])) {
			printf("  -m: The generated DNA string cannot contain any substring consisting of this many consecutive occurrences of the same letter (default to %s). \n", mstring);
			printf("  -n: The number of letters in (or equivalently the length of) the circular DNA string (default to %s). \n", nstring);
		}
	}
	char *mstring2 = strdup(mstring);
	myuint n_ms = get_maxint(mstring2);
	fprintf(stderr, "n_ms = %lu\n", n_ms);
	
	char *nstring2 = strdup(nstring);
	myuint n_ns = get_maxint(nstring2);
	fprintf(stderr, "n_ns = %lu\n", n_ns);
	
    myuint **mn2result = malloc2d(n_ms,n_ns);
		
	myuint **dptableFB = malloc2d(n_ms,n_ns);
	myuint **dptableFB1 = malloc2d(n_ms,n_ns);
	myuint **dptableFB2L1 = malloc2d(n_ms,n_ns);
	myuint **dptableFB2L3 = malloc2d(n_ms,n_ns);
	myuint **dpTableFC = malloc2d(n_ms,n_ns);

	free(mstring2);
	mstring2 = strdup(mstring);
    char *mptr_rest = mstring2;
	char *mptr = NULL;
	while ((mptr = strtok_r(mptr_rest, SEPS, &mptr_rest)) != NULL) {
		myuint m = atoi(mptr);
		
		free(nstring2);
		nstring2 = strdup(nstring);
		char *nptr_rest = nstring2;
		char *nptr = NULL;
        while ((nptr = strtok_r(nptr_rest, SEPS, &nptr_rest)) != NULL) {
            myuint n = atoi(nptr);
			// fprintf(stderr, "With  ms=%s and ns=%s\n", mstring, nstring);
			// fprintf(stderr, "Trying m=%lu and n=%lu\n", m, n);
			
			myuint vFB = FB(m,n,dptableFB);
            myuint vFB1 = FB1(m,n,dptableFB1);
            myuint vFB2L1 = FB2L1(m,n,dptableFB2L1,dptableFB2L3);
            myuint vFB2L3 = FB2L3(m,n,dptableFB2L1,dptableFB2L3);
            myuint vFB2 = FB2(m,n,dptableFB2L1,dptableFB2L3);
            myuint vFC = FC(m,n,dptableFB,dptableFB2L1,dptableFB2L3,dpTableFC);
			
			int debug_level = 1;
			if (debug_level < 1) {
				fprintf(stderr, "FB(%lu, %lu) = %lu\n", m, n, vFB);
				fprintf(stderr, "FB1(%lu, %lu) = %lu\n", m, n, vFB1);
				fprintf(stderr, "FB2L1(%lu, %lu) = %lu\n", m, n, vFB2L1);
				fprintf(stderr, "FB2L3(%lu, %lu) = %lu\n", m, n, vFB2L3);
				fprintf(stderr, "FB2(%lu, %lu) = %lu\n", m, n, vFB2);
				fprintf(stderr, "FC(%lu, %lu) = %lu\n", m, n, vFC);
			}
			// print2d(dptableFB, "dptableFB", n_ms, n_ns);
			// print2d(dptableFB1, "dptableFB1", n_ms, n_ns);
			// print2d(dptableFB2L1, "dptableFB2L1", n_ms, n_ns);
			// print2d(dptableFB2L3, "dptableFB2L3", n_ms, n_ns);
			// print2d(dpTableFC, "dpTableFC", n_ms, n_ns);
			
			myuint eRotRC = FD1(m,n,dptableFB,dptableFB2L1,dptableFB2L3,dpTableFC);
			// fprintf(stderr, "FD(%lu, %lu) = %lu\n", m, n, eRotRC);
#if 0
			eRC, eRot, eRotRC = FD1(m,n,dptableFB,dptableFB2L1,dptableFB2L3,dpTableFC);          
			logging.debug('FB({}, {}) = {}'.format(m, n, vFB))
            logging.debug('FB1({}, {}) = {}'.format(m, n, vFB1))
            logging.debug('FB2L1({}, {}) = {}'.format(m, n, vFB2L1))
            logging.debug('FB2L3({}, {}) = {}'.format(m, n, vFB2L3))
            logging.debug('FB2({}, {}) = {}'.format(m, n, vFB2))
            logging.debug('FC({}, {}) = {}'.format(m, n, vFC))
            logging.info('Finally, FD(m={}, n={}) = {},{},{} (number of reverse-complementation(RC), rotation, and RC-and-rotation equivalent classes)'.format(m, n, eRC, eRot, eRotRC))
            
#endif            
            mn2result[m][n] = eRotRC;
		}
    }
	printf("%% Table-of-results\n");
    for (myuint im = 1 ; im < n_ms; im++) {
        for (myuint in = 1; in < n_ns; in++) {
			const char *latexsep = ((1==in) ? "" : "&");
			if (INIVAL == mn2result[im][in]) {
				printf(" %s NA \t", latexsep);
			} else {
				printf(" %s %lu \t", latexsep, mn2result[im][in]);
			}
		}
		printf("\\\\ \n");
	}

	free(mstring2);
	free(nstring2);
	
	free2d(mn2result, n_ms, n_ns);
		
	free2d(dptableFB, n_ms, n_ns);
	free2d(dptableFB1, n_ms, n_ns);
    free2d(dptableFB2L1, n_ms, n_ns);
	free2d(dptableFB2L3, n_ms, n_ns);
	free2d(dpTableFC, n_ms, n_ns);
}
