# combinatorics
Solutions to problems in combinatorics

combinatoricsCircleSeqs.py and combinatoricsCircleSeqs.c
    Computes the number of DNA sequences satisfying the following two requirements.
      (1) The DNA sequence has exactly n letters. 
      (2) The DNA sequence does not contain any substring consisting of m consecutive and identical letters. 
    If a DNA sequence A can be obtained by either the rotation or the reverse complementation (RC) of another DNA sequence B, 
      then A and B are considered to be equivalent to each other.
